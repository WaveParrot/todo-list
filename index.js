var addButton = document.getElementById('add');
var delallButton = document.getElementById('delete');
var SortIportantButton = document.getElementById('sortimportant');
var FilterButton = document.getElementById('filter');
var UpdateButton = document.getElementById('update');
var inputTask = document.getElementById('new-task');
var inputCat = document.getElementById('category');
var Tasks = document.getElementById('tasks');
var CovidWorld = document.getElementById('COVIDworld');
var CovidRU = document.getElementById('COVIDru');
var ol;
var data;

function createNewElement(task, category, imp) {
    ol = document.createElement('ol');
    var listItem = document.createElement('li');      
    var important = document.createElement('button');
    if (imp)
    {
        important.className = "material-icons important";
        important.innerHTML = "<font class='material-icons' color='red'>bookmark</font>"; 
    } else {
        important.className = "material-icons important";
        important.innerHTML = "<font class='material-icons'>bookmark_border</font>"; 
    }
    var checkbox = document.createElement('input');
    checkbox.type="checkbox";
    checkbox.className = "checkbox";
    var label = document.createElement('label');
    label.innerText = task;
    var input = document.createElement('input');
    input.type = "text";
    var labelcat = document.createElement('label-category');
    labelcat.innerText = category;
    var inputcat = document.createElement('input');   
    inputcat.type = "text-category";
    var plusButton = document.createElement('button');
    plusButton.className = "material-icons add";
    plusButton.innerHTML = "<i class='material-icons'>add</i>";
    var editButton = document.createElement('button');
    editButton.className = "material-icons edit";
    editButton.innerHTML = "<i class='material-icons'>edit</i>";
    var deleteButton = document.createElement('button');
    deleteButton.className = "material-icons delete";
    deleteButton.innerHTML = "<i class='material-icons'>delete</i>";

    listItem.appendChild(checkbox);
    listItem.appendChild(important); 
    listItem.appendChild(label);
    listItem.appendChild(labelcat);   
    listItem.appendChild(input);
    listItem.appendChild(inputcat); 
    listItem.appendChild(plusButton)
    listItem.appendChild(editButton);
    listItem.appendChild(deleteButton); 
    listItem.appendChild(ol);   

    return listItem;
}

function createNewSubTask(text) {   
    var listItem = document.createElement('li');
    var label = document.createElement('label-sub');
    label.innerText = text;
    var input = document.createElement('input');
    input.type = "subtext";
    input.value = text;
    input.placeholder = "Подзадача";
    input.disabled = true;
    var editButton = document.createElement('button');
    editButton.className = "material-icons subedit";
    editButton.innerHTML = "<i class='material-icons'>edit</i>";
    var deleteButton = document.createElement('button');
    deleteButton.className = "material-icons subdelete";
    deleteButton.innerHTML = "<i class='material-icons'>delete</i>";
  
    listItem.appendChild(label); 
    listItem.appendChild(input);
    listItem.appendChild(editButton);
    listItem.appendChild(deleteButton);
    ol.appendChild(listItem);
    return listItem;
}

function addTask() {
    if (inputTask.value) {       
        var listItem = createNewElement(inputTask.value, inputCat.value, false);
        Tasks.appendChild(listItem);
        bindTaskEvents(listItem, NoImportantTask)
        inputTask.value = "";
        inputCat.value = "";
    }
    save();
}
addButton.onclick = addTask;

function AddSubTask() {   
    var olli; 
    var listItem = this.parentNode;
    var ol = listItem.children[9];
    olli = createNewSubTask('');   
    ol.appendChild(olli);
    bindSubTaskEvents(olli);
    save();
}


function deleteTask() {
    var listItem = this.parentNode;
    var li = listItem.parentNode;    
    li.removeChild(listItem);   
    save();
}

function deleteSubTask() {
    var listItem = this.parentNode;
    var li = listItem.parentNode;    
    li.removeChild(listItem);   
    save();
}

function deleteAll() {
    var DelAll = [];
    for (var i = 0; i < Tasks.children.length; i++) {
        if (Tasks.children[i].getElementsByTagName('input')[0].checked){
            DelAll.push(Tasks.children[i])              
        }
    }   
    for (var i = 0; i < DelAll.length; i++) {
        Tasks.removeChild(DelAll[i]);
    }   
    save();
}
delallButton.onclick = deleteAll;

var flag = false;

function SortByImportance() {
    var new_task = Tasks.cloneNode(false);
    var lis = [];
    for(var i = Tasks.childNodes.length; i--;){
        if(Tasks.childNodes[i].nodeName === 'LI')
            lis.push(Tasks.childNodes[i]);
    }
    var SortArr = new Array();
    for (var i = 0; i < Tasks.children.length; i++) {
        SortArr[i] =new Array();
        SortArr[i][1] = i;
        if (Tasks.children[i].getElementsByTagName('font')[0].innerText == 'bookmark_border')
        SortArr[i][0] = false;
        else 
        SortArr[i][0] = true;
    }     
    if (!flag)
    {
        SortArr.sort(); 
        SortArr.reverse();
        flag = true;
    } else {
        SortArr.reverse(); 
    }
    lis.reverse();
    for(var i = 0; i < lis.length; i++) 
        new_task.appendChild(lis[SortArr[i][1]]);
    Tasks.parentNode.replaceChild(new_task, Tasks);
    Tasks = document.getElementById('tasks');    
}
SortIportantButton.onclick = SortByImportance;

function FilteringByCategory() {
    var filter = document.getElementById('filttext');
    for (var i = 0; i < Tasks.children.length; i++) {
        Tasks.children[i].style.display = '';
    }
    for (var i = 0; i < Tasks.children.length; i++) {
        if (Tasks.children[i].getElementsByTagName('label-category')[0].innerText != filter.value
            && filter.value !='') {
                Tasks.children[i].style.display = 'none';
            }
        else if (filter.value =='') {
            Tasks.children[i].style.display = '';
        }
    }  
}
FilterButton.onclick = FilteringByCategory;

function editSubTask() {
    var editButton = this;
    var listItem = this.parentNode;
    var label = listItem.querySelector('label-sub');
    var input = listItem.querySelector('input[type=subtext]');

    var containsClass = listItem.classList.contains('editMode');

    if (containsClass) {
        input.disabled = true;
        label.innerText = input.value;
        editButton.className = "material-icons edit";
        editButton.innerHTML = "<i class='material-icons'>edit</i>";
        save();
    } else {
        input.disabled = false;
        input.value = label.innerText;
        editButton.className = "material-icons save";
        editButton.innerHTML = "<i class='material-icons'>save</i>";
    }
    listItem.classList.toggle('editMode');
}

function editTask() {
    var editButton = this;
    var listItem = this.parentNode;
    var label = listItem.querySelector('label');
    var input = listItem.querySelector('input[type=text]');
    var labelcat = listItem.querySelector('label-category');
    var inputcat = listItem.querySelector('input[type=text-category]');

    var containsClass = listItem.classList.contains('editMode');

    if (containsClass) {
        label.innerText = input.value;
        labelcat.innerText = inputcat.value;
        editButton.className = "material-icons edit";
        editButton.innerHTML = "<i class='material-icons'>edit</i>";
        save();
    } else {
        input.value = label.innerText;
        inputcat.value = labelcat.innerText;
        editButton.className = "material-icons save";
        editButton.innerHTML = "<i class='material-icons'>save</i>";
    }
    listItem.classList.toggle('editMode');
}

function hideIt() {
    for (var i = 0; i < Tasks.children.length; i++) {
        if (Tasks.children[i].getElementsByTagName('input')[0].checked)
            Tasks.children[i].getElementsByTagName('ol')[0].style.display = 'none';
        else
        Tasks.children[i].getElementsByTagName('ol')[0].style.display = '';
    }  
}

function bindSubTaskEvents(listItem) {
    var editButton = listItem.querySelector('button.subedit');
    var deleteButton = listItem.querySelector('button.subdelete');

    editButton.onclick = editSubTask;
    deleteButton.onclick = deleteSubTask;
}

function bindTaskEvents(listItem, importantEvent) {
    var importantButton = listItem.querySelector('button.important');
    var checkbox = listItem.querySelector('input');
    var plusButton = listItem.querySelector('button.add');
    var editButton = listItem.querySelector('button.edit');
    var deleteButton = listItem.querySelector('button.delete');

    importantButton.onclick = importantEvent;  
    checkbox.onclick = hideIt;   
    plusButton.onclick = AddSubTask;
    editButton.onclick = editTask;
    deleteButton.onclick = deleteTask;
}

function ImportantTask() {
    var listItem = this.parentNode;
    var important = listItem.querySelector('button.important');
    important.className = "material-icons important";
    important.innerHTML = "<font class='material-icons'>bookmark_border</font>";
    bindTaskEvents(listItem, NoImportantTask)
    save();
}

function NoImportantTask() {
    var listItem = this.parentNode;
    var important = listItem.querySelector('button.important');
    important.className = "material-icons important";
    important.innerHTML = "<font class='material-icons' color='red'>bookmark</font>";
    bindTaskEvents(listItem, ImportantTask)
    save();
}

function COVID() {
    var CovidArr = "";
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    xhr.addEventListener("readystatechange", function() {
        if(this.readyState === 4) {
            CovidArr = this.responseText;
        }
    });
    xhr.open("GET", "https://api.covid19api.com/summary", false);
    xhr.send();

    var CovidObj = JSON.parse('{"obj":[' + CovidArr + ']}');
    for (var i = 0; i < CovidObj.obj[0].Countries.length; i++) {
        if (CovidObj.obj[0].Countries[i].Country == "Russian Federation"){
            var CovidRu = i;
            break;
        }
    }
    CovidWorld.innerText = "Новые заболевшие в мире - " + CovidObj.obj[0].Global.NewConfirmed;
    CovidRU.innerText = "Новые заболевшие в России - " + CovidObj.obj[0].Countries[CovidRu].NewConfirmed;
}

function save() {
    var TasksArr = [];
    var SubTasksArr = new Array();
    var CategoryArr = [];
    var ImportantArr = [];
    for (var i = 0; i < Tasks.children.length; i++) {
        TasksArr.push(Tasks.children[i].getElementsByTagName('label')[0].innerText);
    }
    for (var i = 0; i < Tasks.children.length; i++) {
        if (Tasks.children[i].getElementsByTagName('ol')[0].children.length != 0) {
        SubTasksArr[i] =new Array();       
            for (var j = 0; j < Tasks.children[i].getElementsByTagName('ol')[0].children.length; j++) {
                SubTasksArr[i][j] = Tasks.children[i].getElementsByTagName('ol')[0].children[j].children[0].innerText;
            }   
        }   
    }
    for (var i = 0; i < Tasks.children.length; i++) {
        CategoryArr.push(Tasks.children[i].getElementsByTagName('label-category')[0].innerText);
    }
    for (var i = 0; i < Tasks.children.length; i++) {
        if (Tasks.children[i].getElementsByTagName('font')[0].innerText == 'bookmark_border')
            ImportantArr[i] = false;
        else 
            ImportantArr[i] = true;
    }
    localStorage.removeItem('todo');
    localStorage.setItem('todo', JSON.stringify({
        Tasks: TasksArr,
        Subtasks: SubTasksArr,
        Category: CategoryArr,
        Important: ImportantArr,
    }));
}

function load(){
    return JSON.parse(localStorage.getItem('todo'));
}
function Update(){
    data=load();
    d = 0;
    while (Tasks.children.length != 0) {
        Tasks.removeChild(Tasks.children[d]);     
    }
    for(var i = 0; i < data.Tasks.length; i++){
        var listItem = createNewElement(data.Tasks[i], data.Category[i], data.Important[i]);
        Tasks.appendChild(listItem);
        if (data.Important[i])
            bindTaskEvents(listItem, ImportantTask);
        else 
            bindTaskEvents(listItem, NoImportantTask);
        flag = false;
    }
    for(var i = 0; i < data.Tasks.length; i++){
        if (data.Subtasks[i] != undefined)
        {
            for(var j = 0; j < data.Subtasks.length; j++){
                if (data.Subtasks[i][j] != undefined)
                {
                    var listItem = createNewSubTask(data.Subtasks[i][j]);
                    var ol = Tasks.children[i].getElementsByTagName('ol')[0]; 
                    ol.appendChild(listItem);
                    bindSubTaskEvents(listItem)
                }
            }
        }
    }
}
Update();
COVID();
